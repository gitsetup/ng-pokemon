import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from "@angular/common/http";
import { AppComponent } from './app.component';

import { PokemonCataloguePage } from './pages/pokemon-catalogue/pokemon.catalogue.page';
import { ProfilePage } from './pages/profile/profile.page';
import { LoginPage } from './pages/login/login.page';
import { AppRoutingModule } from './app-routing.module';
import { LoginFormComponent } from './components/login-form/login-form.component';
import { FormsModule } from '@angular/forms';
import { PokemonCatalogueListComponent } from './components/pokemon-catalogue-list/pokemon-catalogue-list.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CatchPokemonButtonComponent } from './components/catch-pokemon-button/catch-pokemon-button.component';
import { ProfileListComponent } from './components/profile-list/profile-list.component';
import { ProfileListItemComponent } from './components/profile-list-item/profile-list-item.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';



@NgModule({
  declarations: [
    AppComponent,
    PokemonCataloguePage,
    ProfilePage,
    LoginPage,
    LoginFormComponent,
    PokemonCatalogueListComponent,
    NavbarComponent,
    CatchPokemonButtonComponent,
    ProfileListComponent,
    ProfileListItemComponent

  ],
  imports: [
  BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
