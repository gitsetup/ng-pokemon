import { Component, Input, OnInit } from '@angular/core';
import { CatchPokemonService } from './../../services/catch-pokemon.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';

@Component({
  selector: 'app-catch-pokemon-button',
  templateUrl: './catch-pokemon-button.component.html',
  styleUrls: ['./catch-pokemon-button.component.css']
})
export class CatchPokemonButtonComponent implements OnInit {

public isPokemonTaken: boolean = false;

@Input() pokemonName: string  = "";

 public loading: boolean = false;

  constructor( 
  private readonly trainerService:TrainerService,
  private readonly catchPokemonService:CatchPokemonService) { }

  ngOnInit(): void {

    //inputs are resolved
      this.isPokemonTaken = this.trainerService.inPokeDex(this.pokemonName)
  }

// function to catch pokemons, with loading
  onCatchThemAll(): void {
  this.loading = true;

    this.catchPokemonService.addToPokeDex(this.pokemonName)
    .subscribe({
      next: (trainer: Trainer) => {
          console.log("NEXT:", trainer)
         this.loading = false;
         this.isPokemonTaken = this.trainerService.inPokeDex(this.pokemonName);
      },
      error:(error:HttpErrorResponse) => {
          console.log("Error", error.message)
      }
    
    })
   
  }

}
