import { Component, OnInit } from '@angular/core';
import { filter, map, Observable, fromEvent } from 'rxjs';
import { Trainer } from 'src/app/models/trainer.model';
import { TrainerService } from 'src/app/services/trainer.service';
import { StorageUtil } from './../../utils/storage.utils';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css'],
})
/*Handles the Navbar which is only visible if the user is logged in. Lets the user navigate between pokemon-catalogue and Profile.  */
export class NavbarComponent implements OnInit {
  get trainer(): Trainer | undefined {
    return this.trainerService.trainer;
  }

  constructor(private readonly trainerService: TrainerService) {}

  ngOnInit(): void {}
}