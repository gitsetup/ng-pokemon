import { Injectable } from '@angular/core';
import { PokemonCatalogueService } from './pokemon-catalogue.service';
import { TrainerService } from 'src/app/services/trainer.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Trainer } from 'src/app/models/trainer.model';
import { Observable, finalize, tap } from 'rxjs';

const { apiKey, apiTrainers } = environment;

@Injectable({
	providedIn: 'root',
})
export class CatchPokemonService {
	private _loading: boolean = false;

	get loading(): boolean {
		return this._loading;
	}

	constructor(
		private readonly pokemonCatalogueService: PokemonCatalogueService,
		private readonly trainerService: TrainerService,
		private http: HttpClient,
	) {}

	//function to add to our pokedex by name
	public addToPokeDex(pokemonName: string): Observable<Trainer> {
	//if there is no trainer we throw a error
		if (!this.trainerService.trainer) {
			throw new Error(' There is no trainer');
		}

		//getting the trainer for the id
		let trainer: Trainer = this.trainerService.trainer;
		//if there's no pokemon with that name
		if (!pokemonName) {
			throw new Error('No pokemon with name: ' + pokemonName);
		}

		//we cheking for if there is a pokemon with the name in pokedex, if it exist we remove on click otherwise we add
		if (this.trainerService.inPokeDex(pokemonName)) {
			this.trainerService.removeFromPokeDex(pokemonName);
		} else {
			this.trainerService.addToPokeDex(pokemonName);
		}

		const headers = new HttpHeaders({
			'content-type': 'application/json',
			'x-api-key': apiKey,
		});

		this._loading = true;
		//patching to the api 
		return this.http
			.patch<Trainer>(
				`${apiTrainers}/${trainer.id}`,
				{
					pokemon: [...trainer.pokemon],
				},
				{
					headers,
				},
			)
			.pipe(
				tap((updateTrainer: Trainer) => {
					//updating to our latest
					this.trainerService.trainer = updateTrainer;
				}),
				finalize(() => {
					this._loading = false;
				}),
			);
	}

	
}
