import { Injectable } from '@angular/core';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Trainer } from '../models/trainer.model';
import { StorageUtil } from '../utils/storage.utils';

@Injectable({
  providedIn: 'root',
})
export class TrainerService {
  private _trainer?: Trainer;

  get trainer(): Trainer | undefined {
    return this._trainer;
  }
  set trainer(trainer: Trainer | undefined) {
    StorageUtil.storageSave<Trainer>(StorageKeys.Trainer, trainer!);
    this._trainer = trainer;
  }
  constructor() {
    this._trainer = StorageUtil.storageRead<Trainer>(StorageKeys.Trainer);
  }

  //checking if the pokemon exist in the pokeDex with a boolean
  public inPokeDex(pokemonName: string): boolean {
    if (this._trainer) {
      return Boolean(
        this._trainer?.pokemon.find(
          (pokemon: string) => pokemon === pokemonName
        )
      );
    } else {
      return false;
    }
  }

  //adding to the pokedex by name,
  public addToPokeDex(pokemonName: any): void {
    if (this._trainer) {
      this._trainer.pokemon.push(pokemonName);
    }
  }

  //function for removing from pokedex
  public removeFromPokeDex(pokemonName: string): void {
    if (this._trainer) {
      this._trainer.pokemon = this._trainer.pokemon.filter(
        (pokemon: string) => pokemon !== pokemonName
      );
    }
  }
}
